import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MicroSentryModule } from '@glitchtip/micro-sentry-angular';


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MicroSentryModule.forRoot({
      dsn: 'https://51f844cd60fb46d1aa416974413cc8b3@staging.glitchtip.com/1',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
