# micro-sentry

micro-sentry is a minimalist `@sentry/browser`/`@sentry/angular-ivy` SDK alternative. Angular works out of the box. For other platforms, it can be called manually or used to build your own framework specific SDK.

This is a fork of [Tinkoff/micro-sentry](https://github.com/Tinkoff/micro-sentry), which was archived. Thanks Tinkoff team for building the original package!

At this time, the scope is limited on a minimal feature set around Angular. Feel free to contribute or add React/Vue/Whatever versions.

# Angular

Install with `npm i --save @glitchtip/micro-sentry-angular`

With standalone components main.ts:

```ts
import { MicroSentryModule } from "@glitchtip/micro-sentry-angular";
...
    importProvidersFrom(
        MicroSentryModule.forRoot({ dsn: "Your-DSN-Here" })
    ),
```

or with modules app.module.ts:

```ts
import { MicroSentryModule } from "@glitchtip/micro-sentry-angular";
...
  imports: [
    BrowserModule,
    MicroSentryModule.forRoot({
      dsn: 'Your-DSN-Here',
    }),
  ],
```

# JavaScript/TypeScript

Install with `npm i --save @glitchtip/micro-sentry-browser`

```ts
import { BrowserMicroSentryClient } from "@glitchtip/micro-sentry-browser";
const client = new BrowserMicroSentryClient({
  dsn: 'Your-DSN-Here',
});

try {
  // your app code
} catch (e) {
  client.report(e);
}
```


## Upgrade from Tinkoff/micro-sentry

To upgrade, change `@micro-sentry/browser` to `@glitchtip/micro-sentry-browser` (or core, angular)

There should be no functional differences for end users. However our development policies differ.

- We use angular cli instead of nx.
- We mark the minimum supported Angular version, time is better spent fixing bugs than endless updating versions.
- Focus on modern Angular development (standalone components etc). For older version support, use the Tinkoff package.

## Development server

Add your DSN to projects/micro-sentry-test/src/app/app.module.ts

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Contributing

Merge requests and bug reports welcome. Make only feature requests that you intend to contribute to.
